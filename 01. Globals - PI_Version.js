/* PURPOSE
   Explore few global variables of the environment.

   NOTES
   Globals are listed in PI's Object Explorer view under Global.
   We show how to print more complex console messages mixing strings and variables.

   LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function PI_Version() {
   // show the console
   Console.show();
   // print the current PI extended version
   Console.noteln("Core version: ", coreVersionCodename,
      ", v", coreVersionMajor,
      ".",coreVersionMinor,
      ".", coreVersionRelease,
      "-", coreVersionRevision);
}

PI_Version();
