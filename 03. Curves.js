/* PURPOSE
   Implement a parametric usage of the Curves process to perform a Lch saturation
   of an image.

   NOTES
   To ensure that the process is correctly configured, one easy way is to create
   a process instance on the workspace, right click on the instance and select
   "Edit Instance Source Code". This code is automatically genertaed and
   configures the process with the instance's parameters.
   Once the code is imported we can modity the patameters values as needed
   by our script.

   Note that an instance of a process is instantiated with the default values
   for its parameters, thus your script can modify only the parameters that
   need to change letting the remaining parameters unchanged.

   LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This function takes a view and the amount value (between 0 and 1) and
 * applies a linear curves transformation on the C channel of the Lch color
 * space to increase the image saturation.
 */
 function applyCurves(view, amount) {

   // Instantiate the CurvesTransformation process

   var P = new CurvesTransformation;
   P.c = [ // x, y
      [0.00000, 0.00000],
      [(1 - amount), 1.00000],
      [1.00000, 1.00000]
   ];
   P.ct = CurvesTransformation.prototype.Linear;

   // Perform the transformation
   P.executeOn(view);
}

/*
 * Script MAIN function. We assume that the default saturation amount is 0.1
 */
function main() {
   Console.show();

   // check if script is executed in global context
   if (Parameters.isGlobalTarget) {
      Console.criticalln("Script cannot be executed in global context.");
      return;
   }

   // if the script is launched as an instance on a target view?
   if (Parameters.isViewTarget) {

      // Then apply the transfomration to the targetview
      applyCurves(Parameters.targetView, 0.1);

   } else {

      Console.noteln(ImageWindow.activeWindow.mainView.id)

      // Does an active window exist?
      if (ImageWindow.activeWindow.mainView.id) {

         // Then apply the transformation to the main view of the active window
         applyCurves(ImageWindow.activeWindow.mainView, 0.1);

      } else {

         // Error
         Console.criticalln("Script cannot be executed without an active window.");
      }
   }
}

main();
