/* PURPOSE
   Get familiar with the concept of script execution context and how to detect
   the target image of a script.

   NOTES
   In order to be encapsulable, a script must be able to generate instances.
   An instance defines a set of working parameters and control data that
   completely determine the behavior and identity of a process. The Parameters
   JavaScript object allows a script to import and export a set of working parameters.

   PI can launch a script in three different contexts:

   - Direct context: It refers to a script that is being executed without an instance.
                     For example, a script is executed directly by selecting the
                     Execute > Compile and Run menu item from the Script Editor window.

   - Global context: We say that a process instance runs in the global context
                     when it is executed without an explicit reference to a
                     particular image.

   - View context: This refers to an instance being executed with an explicit
                   reference to a target view.

   This script detects and prints its execution context in the console.

   LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function WhichContext() {
   Console.show();

   // Show the content of the Parameters object (for logging purposes)
   Console.writeln(JSON.stringify(Parameters, null, 2));

   if (Parameters.isViewTarget) { // Is a target view defined?
      // then print the target view id
      Console.writeln("Execute on view context [", Parameters.targetView.id.toString(), "]");

   } else if (Parameters.isGlobalTarget) { // is the script launched in the global context?

      Console.writeln("Execute on global context");

   } else { // the script is launched in the direct context (from menu or from the script editor)

      Console.writeln("Execute on direct context");
   }
}

WhichContext();
