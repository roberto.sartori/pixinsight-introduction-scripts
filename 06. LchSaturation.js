/* PURPOSE
   Finalize the Lch Saturation script implementing the script execution.

   NOTES
   With respect the "05. DialogUI.js" script, this script contains:
   1. implements the applyCurves function that performs the Lch saturation
   2. perform the Lch saturation once the dialog is closed

   LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pjsr/Sizer.jsh>          // needed to instantiate the VerticalSizer and HorizontalSizer objects
#include <pjsr/NumericControl.jsh> // needed to instantiate the NumericControl control

// define a global variable containing script's parameters
var LchSaturationParameters = {
   satAmount: 0,
   targetView: undefined
}

/*
 * Construct the script dialog interface
 */
function LchSaturationDialog() {
   this.__base__ = Dialog;
   this.__base__();

   // let the dialog to be resizable by fragging its borders
   this.userResizable = true;

   // set the minimum width of the dialog
   this.scaledMinWidth = 340;

   // create a title area
   // 1. sets the formatted text
   // 2. sets read only, we don't want to modify it
   // 3. sets the background color
   // 4. sets a fixed height, the control can't expand or contract
   this.title = new TextBox(this);
   this.title.text = "<b>Lch Saturation</b><br><br>This script increases" +
                     " the target image saturation by linearly stretching the C " +
                     " channel in Lch color space";
   this.title.readOnly = true;
   this.title.backroundColor = 0x333333ff;
   this.title.minHeight = 80;
   this.title.maxHeight = 80;

   // add a view picker
   // 1. retrieve the whole view list (images and previews)
   // 2. sets the initially selected view
   // 3. sets the selection callback: the target view becomes the selected view
   this.viewList = new ViewList(this);
   this.viewList.getAll();
   LchSaturationParameters.targetView = this.viewList.currentView;
   this.viewList.onViewSelected = function (view) {
      LchSaturationParameters.targetView = view;
   }

   // create the input slider
   // 1. sets the text
   // 2. stes a fixed label width
   // 3. sets the range of the value
   // 4. sets the value precision (number of decimal digits)
   // 5. sets the range of the slider
   // 6. sets a tooltip text
   // 7. defines the behaviour on value change
   this.satAmountControl = new NumericControl(this);
   this.satAmountControl.label.text = "Saturation level:";
   this.satAmountControl.label.width = 60;
   this.satAmountControl.setRange(0, 1);
   this.satAmountControl.setPrecision( 2 );
   this.satAmountControl.slider.setRange( 0, 100 );
   this.satAmountControl.toolTip = "<p>Sets the amount of saturation.</p>";
   this.satAmountControl.onValueUpdated = function( value )
   {
      LchSaturationParameters.satAmount = value;
   };

   // prepare the execution button
   // 1. sets the text
   // 2. sets a fixed width
   // 3. sets the onClick function
   this.execButton = new PushButton(this);
   this.execButton.text = "Execute";
   this.execButton.width = 40;
   this.execButton.onClick = () => {
      this.ok();
   };

   // layout the dialog
   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add(this.title);
   this.sizer.addSpacing(8);
   this.sizer.add(this.viewList);
   this.sizer.addSpacing(8);
   this.sizer.add(this.satAmountControl);
   this.sizer.addSpacing(8);
   this.sizer.add(this.execButton);
   this.sizer.setAlignment(this.execButton, Align_Right);
   this.sizer.addStretch();
}

LchSaturationDialog.prototype = new Dialog;

function main() {

   // hide the console, we don't need it
   Console.hide();

   // create and show the dialog
   let dialog = new LchSaturationDialog;
   dialog.execute();

   // check if a valid target view has been selected
   if (LchSaturationParameters.targetView && LchSaturationParameters.targetView.id) {
      // perform the Lch saturation
      applyCurves(LchSaturationParameters.targetView, LchSaturationParameters.satAmount);
   } else {
      Console.warningln("No target view is specified ");
   }
}

main();
